# Todd's dotfiles

## Usage

### Create symlinks

```bash
stow .
```

### Install Homebrew formulae and casks

```bash
./brew.sh
```
